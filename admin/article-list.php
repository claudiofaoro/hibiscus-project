<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Article List - Admin Panel</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.0/css/bulma.min.css">
</head>

<body>
    <?php include 'navigation-admin.php'; ?>
    <?php
    $path = $_SERVER['DOCUMENT_ROOT'];

    $pathArticles = $path . "/articles";
    $allArticles = array_slice(scandir($pathArticles), 2);

    ?>
    <div class="container">
        <table class="table">
            <tr>
                <th>Title</th>
                <th>Options</th>
            </tr>

            <?php foreach ($allArticles as $item) {
                echo "<tr><td>" . $item . "</td>" . "<td>" . "Elimina" . "</td>" . "</tr>";
            }
            ?>
            <tr>
                <td></td>
            </tr>
        </table>
    </div>

</body>