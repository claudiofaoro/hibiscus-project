<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Add Article - Admin Panel</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.0/css/bulma.min.css">
</head>

<body>
    <?php include 'navigation-admin.php'; ?>
    <div class="container">
        <input class="input" type="text" placeholder="Title">
        <textarea class="textarea has-fixed-size" placeholder="Text"></textarea>
        <button class="button">Submit</button>
    </div>
</body>

</html>